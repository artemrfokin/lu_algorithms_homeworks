from collections import deque

g = {0: [1, 3, 2],
     1: [3, 0],
     2: [0, 3],
     3: [2, 0, 1]}


def find_paths(g, src, dst):
    queue = deque()
    queue.append([src])
    while len(queue):
        path = queue.popleft()
        last = path[-1]
        if last == dst:
            print(path)
            continue
        for v in g[last]:
            if not v in path:
                queue.append(path + [v])

find_paths(g, 1, 2)
