g = {0: [1, 3, 2],
     1: [3, 0],
     2: [0, 3],
     3: [2, 0, 1],
     4: [5],
     5: [4]}

components = {}


def dfs(g, a, g_visited, connected_component_num):
    stack = [a]
    while len(stack) != 0:
        v = stack.pop()
        if v not in g_visited:
            g_visited.add(v)
            components[v] = connected_component_num
            stack.extend(set(g[v]) - g_visited)


if __name__ == '__main__':
    # ...
    g_visited = set()
    connected_component_num = 0
    for k, _ in g.items():
        if k not in g_visited:
            dfs(g, k, g_visited, connected_component_num)
            connected_component_num += 1

    for k, v in components.items():
        print(k, " ", v)
        