grath = [[1, 3, 2], [3, 0], [0, 3], [2, 0, 1]] # 4 nodes grath (rhombus with one diagonal edge)
# grath = [[1, 2], [3, 4, 5], [6, 7], [], [], [], [], []] # tree grath
nowPath = []
correctPaths = []


def is_node_visited(node_index):
    return node_index in nowPath


def investigate_paths(node_number, finish_node):
    nowPath.append(node_number)
    if len(grath[node_number]) != 0:
        for i in range(len(grath[node_number])):
            if grath[node_number][i] == finish_node:
                nowPath.append(grath[node_number][i])
                if nowPath not in correctPaths:
                    print(nowPath)
                    correctPaths.append(nowPath.copy())
                    nowPath.pop()
                    continue
                else:
                    nowPath.pop()
            if is_node_visited(grath[node_number][i]) != True:
                investigate_paths(grath[node_number][i], finish_node)
                nowPath.pop()


investigate_paths(1, 2)
