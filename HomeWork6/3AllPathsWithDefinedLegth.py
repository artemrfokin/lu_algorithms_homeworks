from collections import deque

g = {0: [1, 3, 2],
     1: [3, 0],
     2: [0, 3],
     3: [2, 0, 1]}

g1 = {0: [1, 2],
     1: [0, 3, 4, 5],
     2: [0, 6, 7],
     3: [1],
     4: [1],
     5: [1],
     6: [2],
     7: [2]}


def find_paths(g, src, legth):
    queue = deque()
    queue.append([src])
    while len(queue):
        path = queue.popleft()
        last = path[-1]
        if len(path) == legth + 1:
            print(path)
            continue
        for v in g[last]:
            if not v in path:
                queue.append(path + [v])


for k, _ in g1.items():
    find_paths(g1, k, 3)
