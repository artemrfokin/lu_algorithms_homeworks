grath = [[1, 3, 2], [3, 0], [0, 3], [2, 0, 1]] # 4 nodes grath (rhombus with one diagonal edge)
# grath = [[1, 2], [3, 4, 5], [6, 7], [], [], [], [], []] # tree grath
nowPath = []
correctPaths = []

def isNodeVisited(nodeIndex):
    return nodeIndex in nowPath

def investigate(nodeNumber):
    nowPath.append(nodeNumber)
    print(nodeNumber)
    if len(grath[nodeNumber]) != 0:
        for i in range(len(grath[nodeNumber])):
            if isNodeVisited(grath[nodeNumber][i]) != True:
                investigate(grath[nodeNumber][i])