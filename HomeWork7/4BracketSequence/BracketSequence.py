def is_bracket_sequence_right(seq):
    opening_brackets = ["(", "{", "<", "["]
    closing_brackets = [")", "}", ">", "]"]
    dict = {")": "(",
            "}": "{",
            ">": "<",
            "]": "["}
    arr_seq = list(seq)
    stack = []
    result = True

    for i in range(len(arr_seq)):
        if len(stack) == 0:
            if dict.get(arr_seq[i]) != None:
                result = False
                break

        if arr_seq[i] in opening_brackets:
            stack.append(arr_seq[i])
        elif arr_seq[i] in closing_brackets:
            if dict.get(arr_seq[i]) == stack[-1]:
                stack.pop()
            else:
                return False

    if len(stack) != 0:
        result = False

    return result


sequence = "{()<efw>[,e]}()564<{}(efwe){()<>}>"

print(is_bracket_sequence_right(sequence))
