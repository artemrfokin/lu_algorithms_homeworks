class ListWithTrackingKElement(object):
    def __init__(self, k):
        self.k = k
        self.k_element = None
        self.arr_list = list()
        self.count = 0

    def add(self, element):
        self.arr_list.append(element)
        if len(self.arr_list) == self.k:
            self.k_element = self.arr_list[0]
        if len(self.arr_list) > self.k:
            self.count += 1
            self.k_element = self.arr_list[self.count]

    def remove(self):
        self.arr_list.pop()
        if len(self.arr_list) >= self.k:
            self.count -= 1
            self.k_element = self.arr_list[self.count]
        else:
            self.k_element = None

    def getKElement(self):
        if self.k_element != None:
            return self.k_element
        else:
            raise RuntimeError("Legth of list is lower than k.")


l = ListWithTrackingKElement(3)
l.add(1)
l.add(2)
l.add(3)
l.add(4)
l.remove()

print(l.getKElement())