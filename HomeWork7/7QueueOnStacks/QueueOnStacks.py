class Stack(object):
    def __init__(self):
        self.data = [None, None]

    def push(self, value):
        if self.data[0] == None:
            self.data[0] = value
        else:
            if self.data[1] == None:
                stack = Stack()
                self.data[1] = stack
                stack.push(value)
            else:
                self.data[1].push(value)

    def pop(self):
        if self.data[0] != None:
            result = self.data[0]
            self.data[0] = None
            return result
        else:
            if self.data[1] == None:
                raise IndexError("Queue is empty")
            else:
                return self.data[1].pop()


queue = Stack()
queue.push(1)
queue.push(2)
queue.push(3)
print(queue.pop())
print(queue.pop())
print(queue.pop())
