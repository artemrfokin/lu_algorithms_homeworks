from Node import Node

r = Node(1)
r_left = Node(2)
r_right = Node(3)
r_left_left = Node(4)
r_left_right = Node(5)
r_right_left = Node(6)
r_right_right = Node(7)
r_left_left_left = Node(8)
r_left_left_right = Node(9)
r_left_right_right = Node(10)
r_left_left_right_left = Node(11)
r_left_right_right_right = Node(12)
r_left_right_right_right_left = Node(13)

r.left = r_left
r.right = r_right

r_left.left = r_left_left
r_left.right = r_left_right
r_left.parent = r

r_right.left = r_right_left
r_right.right = r_right_right
r_right.parent = r

r_left_left.left = r_left_left_left
r_left_left.right = r_left_left_right
r_left_left.parent = r_left

r_left_right.right = r_left_right_right
r_left_right.parent = r_left

r_left_left_left.parent = r_left_left

r_left_left_right.left = r_left_left_right_left
r_left_left_right.parent = r_left_left

r_left_right_right.right = r_left_right_right_right
r_left_right_right.parent = r_left_right

r_left_left_right_left.parent = r_left_left_right

r_left_right_right_right.left = r_left_right_right_right_left
r_left_right_right_right.parent = r_left_right_right

r_left_right_right_right_left.parent = r_left_right_right_right

r_right_left.parent = r_right

r_right_right.parent = r_right


def count_level(node):
    count = 0
    if node.parent != None:
        count += 1
        count += count_level(node.parent)
    return count

def find_node_upper(node, levels_up):
    now_node = node
    for i in range(levels_up):
        now_node = now_node.parent
    return now_node


def find_common_parent(node1, node2):
    level1 = count_level(node1)
    level2 = count_level(node2)

    if level1 <= level2:
        high_node = node1
        low_node = node2
        dif = level2 - level1
    else:
        high_node = node2
        low_node = node1
        dif = level1 - level2

    same_level_node = find_node_upper(low_node, dif)

    found = False
    while found != True:
        parent1 = high_node.parent
        parent2 = same_level_node.parent
        if parent1 == parent2:
            found = True
        else:
            high_node = parent1
            same_level_node = parent2

    return parent1


print(r_left)
print(find_common_parent(r_left_right_right_right_left, r_left_left_left))


