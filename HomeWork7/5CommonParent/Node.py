class Node(object):
    def __init__(self, data):
        self.data = data
        self.parent = None
        self.left = None
        self.right = None
