def zip(src):
    exch_cur = 0
    count = 0
    src = list(src)

    for i in range(len(src) - 1):
        if src[i + 1] == src[i]:
            if count == 0:
                exch_cur += 1
                count += 1
                if i == len(src) - 2:
                    src[exch_cur] = count + 1
            else:
                count += 1
                if i == len(src) - 2:
                    src[exch_cur] = count + 1

        else:
            if count > 0:
                src[exch_cur] = count + 1
                exch_cur += 1
                src[exch_cur] = src[i + 1]
                count = 0
            else:
                exch_cur += 1
                src[exch_cur] = src[i + 1]

    return "".join(map(str, src[:exch_cur + 1]))


def unzip(source):
    source = list(source)
    dest = list()
    count = 0
    figure = ""
    for i in range(len(source)):
        if source[i].isdigit() != True:
            dest.append(source[i])
        else:
            figure += source[i]
            if i != len(source) - 1:
                if source[i + 1].isdigit() == False:
                    quantity = int(figure)
                    dest[-1] = dest[-1] * quantity
                    figure = ""
            else:
                quantity = int(figure)
                dest[-1] = dest[-1] * quantity
                figure = ""
    return "".join(map(str, dest))


src = "AAAABBBCCXYZDDDDEEEFFFAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBBB"
dst = "A4B3C2XYZD4E3F3A6B28"

""" Zip assert """
# result = zip(src)
# print(src)
# print(result)
# print(result == dst)


""" Unzip assert"""
# result = unzip(dst)
# print(dst)
# print(result)
# print(src)
# print(src == result)


