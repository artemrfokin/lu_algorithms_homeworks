arr = [1, 2, 3, 4, 5, 6]
arr2 = [1, 3, 4, 5, 6]

def deleted_number(old_arr, new_arr):
    sum1 = 0
    for num in old_arr:
        sum1 += num

    sum2 = 0
    for num in new_arr:
        sum2 += num

    return sum1 - sum2

print(deleted_number(arr, arr2))
