from Node import Node


root = Node(5)

r_left = Node(3)
r_right = Node(8)
root.left = r_left
root.right = r_right

r_left_left = Node(2)
r_left_right = Node(4)
r_left.left = r_left_left
r_left.right = r_left_right

r_right_left = Node(7)
r_right_right = Node(9)
r_right.left = r_right_left
r_right.right = r_right_right

r_right_right_right = Node(11)
r_right.right.right = r_right_right_right


def is_binary_search_tree(r):
    result = False

    if r.data ==  None and r.left == None and r.right == None:
        result = True
    elif r.left == None and r.right == None:
        result = True
    else:
        result_left = True
        result_right = True

        if r.left != None:
            if r.left.data != None:
                if r.left.data <= r.data:
                    result_left = is_binary_search_tree(r.left)
                else:
                    result_left = False
            else:
                result_left = False

        if r.right != None:
            if r.right.data != None:
                if r.right.data >= r.data:
                    result_right = is_binary_search_tree(r.right)
                else:
                    result_right = False
            else:
                result_right = False

        result = result_left == True and result_right == True

    return result


print(is_binary_search_tree(root))
