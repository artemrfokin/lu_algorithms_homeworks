def is_polyndrom(src):
    result = True

    num_letters = 0
    for elem in src:
        if elem.isalpha():
            num_letters += 1

    end_cur = len(src) - 1
    start_cur = 0
    for i in range(num_letters // 2):
        while src[start_cur].isalpha() != True:
            start_cur += 1

        while src[end_cur].isalpha() != True:
            end_cur -= 1

        if src[start_cur].lower() != src[end_cur].lower():
            result = False
            break

        start_cur += 1
        end_cur -= 1

    return result


# source = "Do geese see God?"
source = "Madam, I’m Adam"

print(is_polyndrom(source))

