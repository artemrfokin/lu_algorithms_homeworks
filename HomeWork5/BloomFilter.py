class BloomFilter(object):
    def __init__(self):
        self._bits = []

    def __hash_function1(self, value):
        return value * 11 % len(self._bits)

    def __hash_function2(self, value):
        return value * 23 % len(self._bits)

    def __hash_function3(self, value):
        return value * 37 % len(self._bits)

    def scan_data(self, arr):
        for i in range(len(arr) * 5):
            self._bits.append(0)

        for elem in arr:
            ind1 = self.__hash_function1(elem)
            self._bits[ind1] = 1
            ind2 = self.__hash_function2(elem)
            self._bits[ind2] = 1
            ind3 = self.__hash_function3(elem)
            self._bits[ind3] = 1

    def contains(self, elem):
        ind1 = self.__hash_function1(elem)
        ind2 = self.__hash_function2(elem)
        ind3 = self.__hash_function3(elem)
        return all([self._bits[ind1] == 1, self._bits[ind2] == 1, self._bits[ind3] == 1])

if __name__ == "__main__":
    bloomFilter = BloomFilter()
    arr = [1, 2, 3, 4, 5, 6, 7, 71, 33, 52, 92]

    bloomFilter.scan_data(arr)
    print(bloomFilter.contains(71))
