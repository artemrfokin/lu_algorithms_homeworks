from StackOnArray import StackOnArray
from StackOnList import StackOnList


"""Please uncomment option you need"""
# stack = StackOnArray()
stack = StackOnList()

stack.push(1)
stack.push(2)
print(stack.getAllElements())
print("size:", stack.size())
stack.pop()
stack.pop()
print(stack.getAllElements())
print("size:", stack.size())
print("Is stack empty?", stack.isEmpty())
