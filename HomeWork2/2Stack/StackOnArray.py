class StackOnArray(object):
    def __init__(self):
        self.list = []

    def push(self, elem):
        self.list.append(elem)

    def pop(self):
        return self.list.pop()

    def size(self):
        return len(self.list)

    def getAllElements(self):
        return self.list

    def isEmpty(self):
        return self.size() == 0
