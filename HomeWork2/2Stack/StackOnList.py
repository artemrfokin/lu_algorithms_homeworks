from List.List import List

class StackOnList(object):
    def __init__(self):
        self.list = List()

    def push(self, elem):
        self.list.add(elem)

    def pop(self):
        self.list.remove(self.list.get_size() - 1)

    def size(self):
        return self.list.get_size()

    def getAllElements(self):
        return self.list.getAllElements()

    def isEmpty(self):
        return self.list.get_size() == 0
