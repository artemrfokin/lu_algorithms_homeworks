from List.Node import Node

class List(object):
    def __init__(self):
        self.head = None
        self.listSize = 0


    def add(self, elem):
        if self.listSize != 0:
            current_node = self.head
            while current_node.next != None:
                current_node = current_node.next
            newNode = Node(elem)
            current_node.next = newNode
        else:
            node = Node(elem)
            self.head = node

        self.listSize += 1

    def remove(self, id):
        if self.get_size() != 0 and id <= self.listSize - 1:
            i = 0
            current_node = self.head
            previous_node = self.head

            while i < id:
                previous_node = current_node
                current_node = current_node.next
                i += 1

            if i == 0:
                if self.listSize == 1:
                    self.head = None
                elif self.listSize > 1:
                    self.head = self.head.next
            elif i != 0 and i != self.listSize - 1:
                previous_node.next = current_node.next
            elif i == self.listSize - 1:
                previous_node.next = None

            self.listSize -= 1
        else:
            raise IndexError


    def getAllElements(self):
        list = []

        current_node = self.head
        for i in range(self.listSize):
            list.append(current_node.data)
            current_node = current_node.next
        return list


    def get_size(self):
        return self.listSize

    def reverseList(self):
        currentNode = self.head
        previousNode = None
        nextNode = None
        for i in range(self.listSize):
            if i == 0:
                self.head = None
                nextNode = currentNode.next
                currentNode.next = None
                previousNode = currentNode
                currentNode = nextNode

            elif i > 0 and i < self.listSize - 1:
                nextNode = currentNode.next
                currentNode.next = previousNode
                previousNode = currentNode
                currentNode = nextNode

            elif i == self.listSize - 1:
                currentNode.next = previousNode
                self.head = currentNode
