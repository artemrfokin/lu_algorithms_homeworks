if __name__ == "__main__":

    arr1 = [3, 9, 14, 15, 17]
    arr2 = [4, 5, 8]
    resArr = arr1 + arr2

    i = 0
    j = 0

    while i < len(arr1) and j < len(arr2):
        if arr1[i] < arr2[j]:
            resArr[i + j] = arr1[i]
            i += 1
        else:
            resArr[i + j] = arr2[j]
            j += 1


    while i < len(arr1):
        resArr[i + j] = arr1[i]
        i += 1

    while j < len(arr2):
        resArr[i + j] = arr2[j]
        j += 1

    print(resArr)

"""
Time complexity = O(n)
Memory complexity = O(2n + 2)
"""