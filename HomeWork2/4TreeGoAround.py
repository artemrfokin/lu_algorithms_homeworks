from Tree.Tree import Tree
from Tree.Node import Node

r = Node(5)

r_left = Node(3)
r_right = Node(8)
r.left = r_left
r.right = r_right

r_left_left = Node(2)
r_left_right = Node(4)
r_left.left = r_left_left
r_left.right = r_left_right

r_right_left = Node(7)
r_right_right = Node(9)
r_right.left = r_right_left
r_right.right = r_right_right


def goAround(root):
    list = []

    if root.left != None and root.right != None:
        list.extend(goAround(root.left))
        list.append(root.data)
        list.extend(goAround(root.right))
    elif root.left != None and root.right == None:
        list.extend(goAround(root.left))
        list.append(root.data)
    elif root.left == None and root.right != None:
        list.append(root.data)
        list.extend(goAround(root.right))
    elif root.left == None and root.right == None:
        list.append(root.data)

    return list


print(goAround(r))
