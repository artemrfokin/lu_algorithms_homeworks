if __name__ == "__main__":

    def binSearch(start, finish, goal):
        mid = (start + finish) // 2
        res = -1
        if arr[mid] == goal:
            res = mid
        elif mid != start and mid != finish:
            if arr[mid] > goal:
                res = binSearch(start, mid, goal)
            else:
                res = binSearch(mid, finish, goal)
        return res


    arr = [1, 2, 3, 4, 5]
    aim = 2

    print("Index of goal is", binSearch(0, len(arr), aim))


"""
Time complexity = O(n)
Memory complexity = O(n) + O(log(n)) = O(n)
    Including:
    array - n
    mid  = log(n)
"""