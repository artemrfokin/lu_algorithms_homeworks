def partion(arr, left, right, pivot):
    pivotInd = left
    for i in range(pivotInd, right + 1):
        if arr[i] < pivot:
            arr[i], arr[pivotInd] = arr[pivotInd], arr[i]
            pivotInd += 1
    return pivotInd


def midian(arr, left, right):
    mid = (right + left) // 2
    a = arr[left]
    b = arr[right]
    c = arr[mid]
    dict = {a: left, b: right, c: mid}

    def midianAlg(a, b, c):
        if (a >= b):
            if c >= a:
                return a
            elif b <= c and c <= a:
                return c
            elif c <= b:
                return b
        else:
            if c >= b:
                return b
            elif a <= c and c <= b:
                return c
            elif c <= a:
                return a

    midianFigure = midianAlg(a, b, c)
    return dict.get(midianFigure)


def quickSort(arr, left, right):
    if right - left > 1:
        visualPivotInd = (right + left) // 2
        realPivotInd = midian(arr, left, right)
        if realPivotInd != visualPivotInd:
            x = arr[visualPivotInd]
            arr[visualPivotInd] = arr[realPivotInd]
            arr[realPivotInd] = x
        newPivotInd = partion(arr, left, right, arr[visualPivotInd])
        quickSort(arr, left, newPivotInd - 1)
        quickSort(arr, newPivotInd, right)
    else:
        if arr[left] > arr[right]:
            x = arr[left]
            arr[left] = arr[right]
            arr[right] = x


arr = [16, 0, 40, 4, -8, 4, 2, 13, 19, 9, -7, 2, 8]

quickSort(arr, 0, len(arr) - 1)
print(arr)
