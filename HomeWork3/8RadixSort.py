def radixSort(arr, currentRank, finalRank):

    def rankHandler(arrElem, rank):
        if rank == 0:
            return arrElem % 10
        elif rank >= 1:
            return arrElem % 10 ** (rank + 1) // 10 ** rank

    buckets = []
    for i in range(10):
        buckets.append([])

    for i in range(len(arr)):
        rankDigit = rankHandler(arr[i], currentRank)
        buckets[rankDigit].append(arr[i])

    indexArr = 0
    for i in range(len(buckets)):
        if buckets[i] != []:
            if currentRank > 0:
                radixSort(buckets[i], currentRank - 1, currentRank - 1)

            for j in range(len(buckets[i])):
                arr[indexArr] = buckets[i][j]
                indexArr += 1

    currentRank += 1

    if currentRank <= finalRank:
        radixSort(arr, currentRank, finalRank)

arr = [78, 457, 0, 8199, 839, 4, 100, 355]

radixSort(arr, 0, 3)
print(arr)
