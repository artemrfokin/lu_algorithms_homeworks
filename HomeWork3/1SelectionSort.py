arr = [44, 55, 12, 42, 94, 18, 6, 67]

min_index = 0
min_number = arr[0]

for i in range(0, len(arr) - 1):
    min_index = i
    min_number = arr[i]
    for j in range(i + 1, len(arr)):
        if arr[j] < min_number:
            min_index = j
            min_number = arr[j]

    if min_index != i:
        x = arr[i]
        arr[i] = arr[min_index]
        arr[min_index] = x

print(arr)

"""
Time compelexity - N^2
Memory complexity - n + 4
"""