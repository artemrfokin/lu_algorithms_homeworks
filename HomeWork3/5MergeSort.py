def mergeSort(source, dest, left, right):
    if right - left < 2:
        if dest[left] > dest[right]:
            x = dest[left]
            dest[left] = dest[right]
            dest[right] = x
        return

    mid = (left + right) // 2
    mergeSort(dest, source, left, mid)
    mergeSort(dest, source, mid + 1, right)

    cur1 = left
    cur2 = mid + 1
    for i in range(left, right + 1):
        if cur2 >= right + 1 or cur1 < mid + 1 and source[cur1] <= source[cur2]:
            dest[i] = source[cur1]
            cur1 += 1
        else:
            dest[i] = source[cur2]
            cur2 += 1


dest = [7, 19, 0, 50, -6, 10, 2, 1, 11]
source = dest.copy()

mergeSort(source, dest, 0, len(dest) - 1)
print(dest)
