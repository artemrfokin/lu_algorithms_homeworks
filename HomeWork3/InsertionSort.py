class InsertionSort(object):
    @staticmethod
    def binSearchAlg(arr, start, finish, number, globStart, globFinish):
        mid = (start + finish) // 2
        if arr[mid] == number:
            return mid + 1

        startL0 = globStart
        finishL0 = globFinish

        if mid != startL0 and mid != finishL0:
            if number < arr[mid] and number >= arr[mid - 1]:
                return mid
            elif number > arr[mid] and number <= arr[mid + 1]:
                return mid + 1
            else:
                if arr[mid] < number:
                    return InsertionSort.binSearchAlg(arr, mid, finish, number, startL0, finishL0)
                elif arr[mid] > number:
                    return InsertionSort.binSearchAlg(arr, start, mid, number, startL0, finishL0)
        elif mid == startL0:
            if number < arr[mid]:
                return mid
            elif number > arr[mid]:
                return mid + 1

        elif mid == finishL0:
            if number < arr[mid]:
                if number < arr[mid - 1]:
                    return mid - 1
                return mid
            elif number > arr[mid]:
                return mid + 1

    @staticmethod
    def binSearch(arr, start, finish, number):
        return InsertionSort.binSearchAlg(arr, start, finish + 1, number, start, finish)

    @staticmethod
    def insertNumber(arr, start, finish, recInd):
        x = 0
        y = 0
        number = arr[finish]
        for i in range(start, finish + 1):
            if i == recInd:
                x = arr[recInd]
                arr[recInd] = number
            elif i > recInd:
                y = arr[i]
                arr[i] = x
                x = y

    @staticmethod
    def insertionSort(arr):
        for i in range(1, len(arr)):
            recIndex = InsertionSort.binSearch(arr, 0, i - 1, arr[i])
            if recIndex != i:
                InsertionSort.insertNumber(arr, 0, i, recIndex)


if __name__ == "__main__":
    arr = [8, 7, 5, 9, 1, 6, 2, 4]

    InsertionSort.insertionSort(arr)
    print(arr)
