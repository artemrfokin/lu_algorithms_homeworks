def binSearchAlg(arr, start, finish, number, globStart, globFinish):
    mid = (start + finish) // 2
    if arr[mid] == number:
        return mid + 1

    startL0 = globStart
    finishL0 = globFinish

    if mid != startL0 and mid != finishL0:
        if number < arr[mid] and number >= arr[mid - 1]:
            return mid
        elif number > arr[mid] and number <= arr[mid + 1]:
            return mid + 1
        else:
            if arr[mid] < number:
                return binSearchAlg(arr, mid, finish, number, startL0, finishL0)
            elif arr[mid] > number:
                return binSearchAlg(arr, start, mid, number, startL0, finishL0)
    elif mid == startL0:
        if number < arr[mid]:
            return mid
        elif number > arr[mid]:
            return mid + 1

    elif mid == finishL0:
        if number < arr[mid]:
            if number < arr[mid - 1]:
                return mid - 1
            return mid
        elif number > arr[mid]:
            return mid + 1


def binSearch(arr, start, finish, number):
    return binSearchAlg(arr, start, finish + 1, number, start, finish)


def insertNumber(arr, start, finish, recInd):
    x = 0
    y = 0
    number = arr[finish]
    for i in range(start, finish + 1):
        if i == recInd:
            x = arr[recInd]
            arr[recInd] = number
        elif i > recInd:
            y = arr[i]
            arr[i] = x
            x = y


def insertionSort(arr, left, right):
    for i in range(left + 1, right + 1):
        recIndex = binSearch(arr, left, i - 1, arr[i])
        if recIndex != i:
            insertNumber(arr, left, i, recIndex)


def timSortAlg(source, dest, left, right):
    if right - left < 5:
        insertionSort(dest, left, right)
        return

    mid = (left + right) // 2
    timSortAlg(dest, source, left, mid)
    timSortAlg(dest, source, mid + 1, right)

    cur1 = left
    cur2 = mid + 1
    for i in range(left, right + 1):
        if cur2 >= right + 1 or cur1 < mid + 1 and source[cur1] <= source[cur2]:
            dest[i] = source[cur1]
            cur1 += 1
        else:
            dest[i] = source[cur2]
            cur2 += 1


def timSort(dest):
    source = dest.copy()
    timSortAlg(source, dest, 0, len(dest) - 1)


arr = [7, 19, 0, 50, -6, 10, 2, 1, 11, 18, -4, 33, 25, 2, 88, -14, 8, 71, 5]

timSort(arr)
print(arr)
