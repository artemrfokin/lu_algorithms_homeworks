class BatchHandler(object):
    def __init__(self, file, leftInd, rightInd, batchSize):
        self.file = file
        self.cur = leftInd
        self.restToRead = self.restToReadInit(leftInd, rightInd)
        self.batchSize = batchSize // 2
        self.buffer = ""
        self.numbers = []


    def restToReadInit(self, leftInd, rightInd):
        if leftInd == 0:
            return rightInd - leftInd + 1
        elif leftInd == rightInd:
            return 1
        else:
            return rightInd - leftInd

    def isEmpty(self):
        return self.restToRead == 0

    def getNextFifure(self):
        if len(self.numbers) == 0:
            while self.restToRead != 0:
                numBytesToReadNow = 0
                if self.restToRead <= self.batchSize:
                    numBytesToReadNow = self.restToRead
                else:
                    numBytesToReadNow = self.batchSize
                self.restToRead -= numBytesToReadNow

                self.file.seek(self.cur)
                batch = "-"
                batch = self.file.read(numBytesToReadNow)
                self.buffer += batch
                self.cur += numBytesToReadNow

                nowFigures = self.buffer.split(" ")
                for el in nowFigures:
                    el = el.strip()

                if self.restToRead != 0:
                    if len(nowFigures) > 1:
                        for i in range(0, len(nowFigures) - 1):
                            if nowFigures[i] != "":
                                self.numbers.append(int(nowFigures[i]))
                        self.buffer = nowFigures[len(nowFigures) - 1]
                        break
                    else:
                        continue
                        # self.numbers.append(int(nowFigures[0]))
                        # self.buffer = ""
                        # break

            if self.restToRead == 0:
                nowFigures = self.buffer.split(" ")
                for el in nowFigures:
                    el = el.strip()

                if nowFigures[len(nowFigures) - 1] != "":
                    for i in range(0, len(nowFigures)):
                        self.numbers.append(int(nowFigures[i]))
                    self.buffer = ""

        if len(self.numbers) != 0:
            result = self.numbers[0]
            del self.numbers[0]
        else:
            if self.restToRead == 0:
                result = None
            else:
                result = self.getNextFifure()
        return result





