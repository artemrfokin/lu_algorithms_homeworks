from InsertionSort import InsertionSort
from BatchHadler import BatchHandler

class ExteranlMemorySorter(object):
    def __init__(self):
        self.inFile = open('inText.txt', 'r+', encoding='utf-8')
        self.batchSize = 5

        self.cur = 0
        self.writtenBytes = 0

        self.writtenBatchesList = []
        self.writtenBatchInd = 0


    def fillInputFile(self):
        fileToFill = open('inText.txt', 'w', encoding='utf-8')
        arrDefault = [7, 19, 0, 50, -6, 10, 2, 1, 11, 18, -4, 33, 25, 2, 88, 88, -14, 8, 71, 5, -9, 21, 661, 400, 36]
        fileToFill.write(" ".join(map(str, arrDefault)))
        fileToFill.close()


    def addWrittenBatchToList(self, start, finish):
        self.writtenBatchesList.append([start, finish])


    def reWriteFile(self, outString):
        self.inFile.seek(self.writtenBytes)
        self.inFile.write(outString)

        self.addWrittenBatchToList(self.writtenBytes, self.writtenBytes + len(outString) - 1)
        self.writtenBytes += len(outString)


    def firstStepOrdering(self):
        batch = "empty string"
        arr = []
        buffer = ""

        while batch != "":
            self.inFile.seek(self.cur)
            batch = self.inFile.read(self.batchSize)
            self.cur += self.batchSize
            print("batch", batch)
            buffer += batch
            nowFigures = buffer.split(" ")
            for i in range(len(nowFigures)):
                nowFigures[i] = nowFigures[i].strip()
            print("nowFigures", nowFigures)

            if batch != "":
                for i in range(0, len(nowFigures) - 1):
                    arr.append(int(nowFigures[i]))
                buffer = nowFigures[len(nowFigures) - 1]
            else:
                for i in range(0, len(nowFigures)):
                    arr.append(int(nowFigures[i]))

            if batch != "":
                if len(arr) > 1:
                    InsertionSort.insertionSort(arr)
                    outString = " ".join(map(str, arr))
                    outString += " "
                    print("Figures which ready to out File", arr)
                    print("outString", outString)
                    self.reWriteFile(outString)
                    arr.clear()
            else:
                InsertionSort.insertionSort(arr)
                print("Figures which ready to out File", arr)
                outString = " ".join(map(str, arr))
                outString += " "
                print("outString", outString)
                self.reWriteFile(outString)
                arr.clear()
        self.inFile.close()


    def copyFile(self, file1, start, finish, file2Name):
        file2 = open(file2Name, 'w', encoding="utf-8")
        restToWrite = finish - start
        cur = start
        while restToWrite > 0:
            nowToRead = 0
            if restToWrite <= self.batchSize:
                nowToRead = restToWrite
            else:
                nowToRead = self.batchSize
            restToWrite -= self.batchSize
            file1.seek(cur)
            inStr = file1.read(nowToRead)
            cur += nowToRead
            file2.write(inStr)
        file2.close()


    def mergeTwoBatches(self, firstBatchInd, seconBatchInd):
        firstBatch = self.writtenBatchesList[firstBatchInd]
        seconBatch = self.writtenBatchesList[seconBatchInd]

        self.inFile = open('inText.txt', 'r+', encoding='utf-8')
        cur = firstBatch[0]
        self.copyFile(self.inFile, firstBatch[0], firstBatch[1], 'tempFile.txt')
        tempFile = open('tempFile.txt', 'r+', encoding='utf-8')

        batchHadler1 = BatchHandler(tempFile, 0, firstBatch[1] - firstBatch[0], self.batchSize)
        batchHadler2 = BatchHandler(self.inFile, seconBatch[0], seconBatch[1], self.batchSize)

        # if batchHadler1.isEmpty() is not True:
        figure1 = batchHadler1.getNextFifure()
        # if batchHadler2.isEmpty() is not True:
        figure2 = batchHadler2.getNextFifure()

        while figure1 != None or figure2 != None:
            if figure2 == None or figure1 != None and figure1 < figure2:
                outString = str(figure1) + " "
                figure1 = batchHadler1.getNextFifure()
            else:
                outString = str(figure2) + " "
                figure2 = batchHadler2.getNextFifure()

            self.inFile.seek(cur)
            self.inFile.write(outString)
            cur += len(outString)

        self.writtenBatchesList[firstBatchInd] = [firstBatch[0], seconBatch[1]]
        print(self.writtenBatchesList)
        self.inFile.close()
        tempFile.close()

    def merge(self):
        while len(self.writtenBatchesList) > 1:
            for i in range(len(self.writtenBatchesList) // 2):
                self.mergeTwoBatches(i * 2, i * 2 + 1)

            n = len(self.writtenBatchesList)
            delIndex = 0
            for i in range(len(self.writtenBatchesList) // 2):
                if n % 2 == 0:
                    if i == 0:
                        delIndex = n - 1
                    else:
                        delIndex -= 2
                elif n % 2 != 0:
                    if i == 0:
                        delIndex = n - 2
                    else:
                        delIndex -= 2
                del self.writtenBatchesList[delIndex]

            print(self.writtenBatchesList)


sorter = ExteranlMemorySorter()
sorter.fillInputFile()
sorter.firstStepOrdering()
print(sorter.writtenBatchesList)
sorter.merge()




# print("buffer", buffer)
# print("arr", arr)



