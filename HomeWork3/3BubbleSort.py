def bubbleSort(arr):
    for i in range(0, len(arr) - 1):
        for j in range(1, len(arr) - i):
            if arr[j] < arr[j - 1]:
                x = arr[j - 1]
                arr[j - 1] = arr[j]
                arr[j] = x


arr = [5, 1, 0, 55, 4, 2, 8]

bubbleSort(arr)
print(arr)
