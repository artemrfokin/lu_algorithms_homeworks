def countSort(arr):
    counter = [0] * 11
    for el in arr:
        counter[el] += 1

    indexArr = 0
    for i in range(len(counter)):
        if counter[i] >= 1:
            for j in range(counter[i]):
                arr[indexArr] = i
                indexArr += 1


arr = [8, 5, 0, 2, 1, 3, 10, 3, 2, 5, 6, 3]

countSort(arr)
print(arr)
