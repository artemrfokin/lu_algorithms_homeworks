if __name__ == "__main__":
    inputList = [5, 4, 0, 18, 3, 4, 7, 6]
    oddFigure = 4
    outputList = []

    for i in range(0, len(inputList)):
        if inputList[i] != oddFigure:
            outputList.append(inputList[i])

    print(outputList)

"""
Time complexity = O(n)
Memory complexity = O(2n)
    Including:
    input list - n
    output list = n
"""