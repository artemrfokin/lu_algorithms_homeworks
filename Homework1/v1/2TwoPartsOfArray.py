if __name__ == "__main__":
    inList = [-2, 6, 2, 0, -1, 7, 4, 3, 5]
    pivot = 4

    outList = []
    higherList = []

    for i in range(0, len(inList)):
        if inList[i] < pivot:
            outList.append(inList[i])
        else:
            higherList.append(inList[i])

    outList.extend(higherList)
    print(outList)

"""
Time complexity = O(n)
Memory complexity = O(2n)
    Including:
    input list - n
    output list + higher list = n
"""