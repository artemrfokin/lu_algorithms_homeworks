if __name__ == "__main__":
    inList = [10, 2, 9, 7, 0]

    minN = inList[0]
    maxN = inList[0]
    mid = 0
    avrNumber = 0
    bestDiff = avrNumber

    for i in range(1, len(inList)):
        if inList[i] <= minN:
            minN = inList[i]
        if inList[i] >= maxN:
            maxN = inList[i]

    mid = (maxN - minN) // 2
    bestDiff = mid

    for i in range(0, len(inList)):
        if abs(inList[i] - mid) <= bestDiff:
            bestDiff = abs(inList[i] - mid)
            avrNumber = inList[i]

    print("Min =", minN, "Max =", maxN, "Middle =", mid)
    print("Average number is", avrNumber)


"""
Time complexity = O(2n)  
Memory complexity = O(2n + 5)    
"""
