if __name__ == "__main__":

    numList = [5, 0, 18]

    minValue = 0
    medValue = 0
    maxValue = 0

    for i in range(0, len(numList)):
        if i == 0:
            minValue = numList[i]
            medValue = numList[i]
            maxValue = numList[i]
        else:
            if numList[i] < minValue:
                medValue = minValue
                minValue = numList[i]
            elif numList[i] >= maxValue:
                medValue = maxValue
                maxValue = numList[i]

    print(medValue)

"""
Time complexity = O(n)
Memory complexity = O(2n)
    Including:
    input list - n
    3 varuables = n
"""