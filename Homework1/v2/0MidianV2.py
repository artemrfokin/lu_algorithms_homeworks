if __name__ == "__main__":

    def midianThreeFigures(a, b, c):
        if (a >= b):
            if c >= a:
                return a
            elif b <= c and c <= a:
                return c
            elif c <= b:
                return b
        else:
            if c >= b:
                return b
            elif a <= c and c <= b:
                return c
            elif c <= a:
                return a


    a = 77
    b = 1
    c = 45

    print(midianThreeFigures(a, b, c))

"""
Time complexity = O(4)
Memory complexity = O(3)
"""