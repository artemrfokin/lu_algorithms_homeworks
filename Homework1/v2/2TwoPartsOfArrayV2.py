if __name__ == "__main__":
    inList = [8, 19, 7, 6, 4, -1, 1, 2, 3, 5]
    pivot = 4
    replaceIndex = 0
    replaceNumbers = 0

    for i in range(len(inList)):
        if inList[i] >= pivot:
            replaceNumbers += 1
            if replaceNumbers == 1:
                replaceIndex = i
        else:
            currentNumber = inList[i]
            inList[i] = inList[replaceIndex]
            inList[replaceIndex] = currentNumber
            replaceIndex += 1

    print(inList)

"""
Time complexity = O(n)
Memory complexity = O(n + 3)
"""