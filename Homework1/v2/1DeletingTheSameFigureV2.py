if __name__ == "__main__":

    arr = [4, 44, 3, 18, 5, 4, 7, 6]
    oddFigure = 4
    numOfOddFigures = 0
    replaceIndex = 0

    for i in range(len(arr)):
        if arr[i] == oddFigure:
            numOfOddFigures += 1
            if numOfOddFigures == 1:
                replaceIndex = i
        else:
            if numOfOddFigures > 0:
                arr[replaceIndex] = arr[i]
                arr[i] = oddFigure
                replaceIndex += 1
    print(arr)

    for i in range(numOfOddFigures):
        arr.pop()

    print(arr)


"""
Time complexity = O(2n)
Memory complexity = O(n + 2)
"""
