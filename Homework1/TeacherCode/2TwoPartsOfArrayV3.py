if __name__ == "__main__":
    arr = [8, 19, 7, 6, -1, 4, 1, 2, 3, 5]
    pivot = 4
    pivotPosition = 0

    for i in range(len(arr)):
        if arr[i] < pivot:
            arr[i], arr[pivotPosition] = arr[pivotPosition], arr[i]
            pivotPosition += 1
    print(pivotPosition)
    # for i in range(pivotPosition, len(arr)):
    #     if arr[i] == pivot:
    #         arr[i], arr[pivotPosition] = arr[pivotPosition], arr[i]
    #         pivotPosition += 1

    print(arr)

"""
Time complexity = O(n)
Memory complexity = O(n + 3)
"""