if __name__ == "__main__":

    arr = [4, 44, 3, 18, 5, 4, 7, 6]
    oddFigure = 4
    k = 0

    for i in range(len(arr)):
        if arr[i] != oddFigure:
            arr[k] = arr[i]
            k += 1

    print(arr[:k])



"""
Time complexity = O(n + 1)
Memory complexity = O(2n + 2)
"""
