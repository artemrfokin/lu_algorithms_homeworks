class Heap(object):
    def __init__(self):
        self.arr = []
        self.arrLength = 0

    def get_parent_ind(self, index):
        return (index - 1) // 2

    def get_left_child_ind(self, index):
        return index * 2 + 1

    def get_right_child_ind(self, index):
        return index * 2 + 2

    def add(self, figure):
        self.arr.append(figure)
        self.arrLength += 1
        newFigureInd = len(self.arr) - 1

        while True:
            parentInd = self.get_parent_ind(newFigureInd)
            if newFigureInd != 0:
                if self.arr[parentInd] < self.arr[newFigureInd]:
                    self.arr[parentInd], self.arr[newFigureInd] = self.arr[newFigureInd], self.arr[parentInd]
                    newFigureInd = parentInd
                    continue
                else:
                    break
            else:
                break

    def pop(self):
        if self.is_empty() != True:
            return self.arr[0]
        else:
            raise RuntimeError("Heap is empty")

    def remove(self):
        if self.is_empty() != True:

            if self.arrLength > 1:
                self.arr[0] = self.arr[self.arrLength - 1]
                self.arrLength -= 1
            else:
                self.arrLength -= 1
                return

            parentInd = 0
            while True:
                leftChildInd = self.get_left_child_ind(parentInd)
                righChildInd = self.get_right_child_ind(parentInd)

                if leftChildInd > self.arrLength - 1:
                    break

                if leftChildInd == self.arrLength - 1:
                    if self.arr[leftChildInd] > self.arr[parentInd]:
                        self.arr[leftChildInd], self.arr[parentInd] = self.arr[parentInd], self.arr[leftChildInd]
                        parentInd = leftChildInd
                        continue
                    else:
                        break

                highestChildInd = leftChildInd if self.arr[leftChildInd] >= self.arr[righChildInd] else righChildInd
                if self.arr[highestChildInd] > self.arr[parentInd]:
                    self.arr[highestChildInd], self.arr[parentInd] = self.arr[parentInd], self.arr[highestChildInd]
                    parentInd = highestChildInd
                else:
                    break

        else:
            raise RuntimeError("Heap is empty")


    def is_empty(self):
        return len(self.arr) == 0

    def get_size(self):
        return self.arrLength

    def showAllElements(self):
        newArr = []
        for i in range(self.arrLength):
            newArr.append(self.arr[i])
        print(newArr)

    def sort(self, ext_arr):
        heap = Heap()
        for elem in ext_arr:
            heap.add(elem)
        for i in range(len(ext_arr)):
            ext_arr[i] = heap.pop()
            heap.remove()

